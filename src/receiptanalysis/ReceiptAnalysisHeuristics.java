package receiptanalysis;

import java.io.File;
import java.util.List;

import org.opencv.core.Mat;

interface ReceiptAnalysisHeuristics {
	public List<List<String>> receiptAnalysis(Mat receiptImage, File directory);
}
