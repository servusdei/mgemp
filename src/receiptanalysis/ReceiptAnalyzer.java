package receiptanalysis;

import java.io.File;
import java.util.List;

import org.opencv.core.Mat;

public class ReceiptAnalyzer {
	
	private ReceiptAnalysisHeuristics rah;
	
	public ReceiptAnalyzer(ReceiptAnalysisHeuristics rah) {
		this.rah = rah;
	}
	
	public List<List<String>> analyzeReceipt(Mat receiptImage, File directory){
		return rah.receiptAnalysis(receiptImage, directory);
	}

}
