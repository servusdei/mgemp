package receiptanalysis;

import java.io.File;
import java.io.IOException;

abstract class ReceiptAnalysis {
	
	protected abstract String imageToText(File regionDirectory) throws IOException, InterruptedException;

}
