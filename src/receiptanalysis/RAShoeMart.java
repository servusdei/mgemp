package receiptanalysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import iputils.IPToolbox;
import iputils.regions.RegionOfInterest;

public class RAShoeMart extends ReceiptAnalysis implements ReceiptAnalysisHeuristics {

	@Override
	public List<List<String>> receiptAnalysis(Mat receiptImage, File directory) {
		Mat base = receiptImage;
		Mat paperMask = null;
		List<MatOfPoint> contours = null;
		List<RegionOfInterest> listOfROIs = null;
		List<Mat> listOfMatROIs = null;
		List<RegionOfInterest> preprocROIs = null;
		
		Map<File, RegionOfInterest> roiMap = new HashMap<>();
		
		paperMask = IPToolbox.getPaperDetectMask(base);
		contours = IPToolbox.getMatContours(paperMask);
		
		Mat histeq = new Mat();
		Imgproc.cvtColor(base, histeq, Imgproc.COLOR_BGR2GRAY);
		Imgproc.equalizeHist(histeq, histeq);
		
		listOfROIs = IPToolbox.getROIFromContoursAsROIs(histeq, contours);
		processROIs(listOfROIs);
		
		RegionOfInterest.writeLargeROIsToDirectory(listOfROIs, directory, "region", 1);
		
		List<List<String>> mainList;
		
		
		try {
			mainList = parseReceipt(listOfROIs, base);
			return mainList;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
		 
	}
	
	private List<List<String>> parseReceipt(List<RegionOfInterest> rois, Mat base) throws IOException, InterruptedException {
		List<List<String>> mainList = new ArrayList<>();
		List<String> priceList = new ArrayList<>();
		List<String> itemList = new ArrayList<>();
		List<String> quantityList = new ArrayList<>();
		
		final String itemsOnly = "[0-9]\\.[0-9]";
		final String subtotal = "\\.*S[UV]B[IT]\\.*";
		final String quantity = "\\.*[0-9]+@\\.*";
		final String price = "\\.*[0-9]+.[0-9]+\\.*";
		
		Pattern patItemsOnly = Pattern.compile(itemsOnly);
		Pattern patSubtotal = Pattern.compile(subtotal);
		Pattern patQuantity = Pattern.compile(quantity);
		Pattern patPrice = Pattern.compile(price);
		Matcher matcher = null;
		
		boolean parsing = false;
		
		final int yThresh = 0;
		
		
		/*
		 * Process
		 */
		
		List<RegionOfInterest> itemSideROI = new ArrayList<>();
		List<RegionOfInterest> priceSideROI = new ArrayList<>();
		
		for(int i = 0; i < rois.size(); i++) {
			RegionOfInterest roi = rois.get(i);
			String ocrOutput = imageToText(roi.getAsFile());
			
			if (ocrOutput == null || ocrOutput.equals("")) continue;
			else ocrOutput = ocrOutput.trim();

			matcher = patSubtotal.matcher(ocrOutput);
			if (matcher.find()) break;
			else if (ocrOutput.equals("PHP")) parsing = true;
			
			if(parsing) {
				if(roi.getX() < (base.width() / 2)) itemSideROI.add(roi);
				else priceSideROI.add(roi);
			}
			
		}
		
		for(int i = 0; i < itemSideROI.size(); i++) {
			RegionOfInterest itemROI = itemSideROI.get(i);
			RegionOfInterest prevItemROI = null;
			String prevItem = null;
			
			if(i > 0) prevItemROI = itemSideROI.get(i-1);
			if(prevItemROI != null) prevItem = imageToText(prevItemROI.getAsFile());
			
			if(!(prevItem == null || prevItem.equals(""))) {
				prevItem = prevItem.trim();
				if(prevItem.contains("@")) {
					String quantityString = prevItem;
					String itemPrice = "0.00";
					
					matcher = patPrice.matcher(quantityString);
					
					if(matcher.find()) itemPrice = matcher.group();
					
					String item = imageToText(itemROI.getAsFile()).trim();
					
					itemList.add(item);
					priceList.add(itemPrice);
					quantityList.add((Character.toString(quantityString.charAt(0))));
					
					continue;
				}
			}
			
			int itemROIHeight = itemROI.getY() + itemROI.getMatRegion().height();
			int itemROIYCenter = itemROIHeight - (itemROI.getMatRegion().height() / 2);
			
			for (int j = 0; j < priceSideROI.size(); j++) {
				RegionOfInterest priceROI = priceSideROI.get(j);
				int priceROIHeight = priceROI.getY() + priceROI.getMatRegion().height();
				
				boolean inRange = (itemROIYCenter > priceROI.getY() - yThresh) && (itemROIYCenter < priceROIHeight + yThresh);
				if(inRange) {
					itemList.add(imageToText(itemROI.getAsFile()).trim());
					priceList.add(imageToText(priceROI.getAsFile()).trim());
					quantityList.add("1");
				}
			}
		}
		
		mainList.add(itemList);
		mainList.add(priceList);
		mainList.add(quantityList);
		
		return mainList;
	}
	
	
	// from notes: 3% unsharp mask 1% contrast stretch
	private void processROIs(List<RegionOfInterest> listOfROIs) {
		for (int i = 0; i < listOfROIs.size(); i++) {
			RegionOfInterest roi = listOfROIs.get(i);
			Mat mat = roi.getMatRegion();
			
			int top = (int) (mat.rows() * 0.1); 
			int bottom = top;
			int left =  (int) (mat.cols() * 0.1);
			int right = left;
			Core.copyMakeBorder(mat, mat, top, bottom, left, right, Core.BORDER_CONSTANT, new Scalar(255));
			
			mat = IPToolbox.unsharpMask(mat, new Size(9,9), 3d);
			if(mat.channels() > 1) Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
			Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_OTSU);
			mat = IPToolbox.deskew(mat);
			
			roi.setMatRegion(mat);
		}

	}

	@Override
	protected String imageToText(File regionDirectory) throws IOException, InterruptedException {
		final String COMMAND = String.format(
				"cmd.exe /c tesseract.exe --tessdata-dir C:\\tesseract-Win64\\tessdata %s stdout --psm 7 -c "
				+ "textord_tabfind_find_tables=0 "
				+ "load_system_dawg=0 "
				+ "load_freq_dawg=0 "
				+ "load_unambig_dawg=0 "
				+ "load_punc_dawg=0 "
				+ "load_number_dawg=0 "
				+ "load_fixed_length_dawgs=0 "
				+ "load_bigram_dawg=0 "
				+ "wordrec_enable_assoc=0 "
				+ "textord_tabfind_vertical_text=0 "
				+ "tessedit_char_whitelist=\"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ<\"",
				regionDirectory.getAbsolutePath());
		
		
		Process p = null;
		p = Runtime.getRuntime().exec(COMMAND);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		String line = "";
		String output = "";
		while ((line = reader.readLine()) != null) {
			output += (line + "\n");
		}

		p.waitFor();

		return output;
	}

}
