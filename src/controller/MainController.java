package controller;

import java.io.File;

import javafx.scene.image.Image;
import model.Model;
import view.View;

public class MainController {
	
	private Model model;
	private View view;
	private static MainController controller;
	
	private File path = null;
	private ImageProcessingController ipc;
	
	public static MainController getInstance(Model model, View view) {
		return controller == null ? controller = new MainController(model, view) : controller;
	}
	
	private MainController(Model model, View view) {
		this.model = model;
		this.view = view;
		
		ipc = new ImageProcessingController();
		
		setActions();
	}
	
	private void setActions() {
		view.getBtnOpenImg().setOnAction((e) -> {
			path = view.invokeFileChooser();
			if(path != null) {
				view.setImageviewImage(path.toURI().toString());
			}
			else {
				view.setImageviewImage("");
			}
		});
		
		view.getBtnProcessImg().setOnAction((e) -> {
			if(path != null) {
				/*model.setImagePath(path);
				model.setRegionPath("C:\\regions");*/
				
				Image result = ipc.startProcess(path, new File("C:\\regions\\"));
				view.setImageviewImage(result);
				path = null;
				
				/*try {
					Desktop.getDesktop().open(new  File("C:\\regions"));
				} catch (IOException e1) {
					e1.printStackTrace();
				}*/
			}
		});
	}

}
