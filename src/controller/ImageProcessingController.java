package controller;

import java.io.File;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import iputils.IPToolbox;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import receiptanalysis.RAShoeMart;
import receiptanalysis.ReceiptAnalyzer;

public class ImageProcessingController {
	
	public Image startProcess(File imagePath, File tempDir) {
		Mat image = Imgcodecs.imread(imagePath.getAbsolutePath());
		Mat base = image.clone();
		
		// while image area is not optimal downscale it
		/*while(base.width() > 1728 && base.height() > 2304) {
			Imgproc.pyrDown(base, base);
		}*/
		
		Imgproc.pyrDown(base, base);
		
		ReceiptAnalyzer ra = new ReceiptAnalyzer(new RAShoeMart());
		List<List<String>> mainList = ra.analyzeReceipt(base, tempDir);
		
		List<String> itemList = mainList.get(0);
		List<String> priceList = mainList.get(1);
		List<String> quantityList = mainList.get(2);
		
		
		System.out.println("item:\t\t\t\t\tprice:\t\t\t\tquantity:");
		for(int i = 0; i < itemList.size() && i < priceList.size(); i++) {
			System.out.println(itemList.get(i) + "\t\t\t" 
					+ priceList.get(i) + "\t\t\t\t" 
					+ quantityList.get(i));
		}
		
		
		
		//draw = IPToolbox.drawMatContours(base, contours, new Scalar(0,255,0));
		//return SwingFXUtils.toFXImage(IPToolbox.mat2Img(draw), null);
		
		return SwingFXUtils.toFXImage(IPToolbox.mat2Img(base), null);
	}
	
	
	/*
	 * Debugging functions 
	 */
	
	@SuppressWarnings("unused")
	private Mat debugProcessROI(Mat roi) {
		Mat mat = roi;

		Imgproc.resize(mat, mat, new Size(0, 0), 5, 5, Imgproc.INTER_LINEAR);
		mat = IPToolbox.unsharpMask(mat, new Size(0,0), 3.0);
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
		Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_OTSU);
		Core.bitwise_not(mat, mat);
		//Imgproc.erode(mat, mat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_OPEN, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
		Core.bitwise_not(mat, mat);
		int top = (int) (mat.rows() * 0.1); 
		int bottom = top;
		int left =  (int) (mat.cols() * 0.1);
		int right = left;
		Core.copyMakeBorder(mat, mat, top, bottom, left, right, Core.BORDER_CONSTANT, new Scalar(255,255,255));
		mat = IPToolbox.deskew(mat);
		
		return mat;
	}

}
