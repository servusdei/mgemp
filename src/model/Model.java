package model;

public class Model {

	/*private String imagePath;
	private String regionPath;

	ArrayList<MatOfPoint> contour;

	public void setImagePath(File imagePath) {
		this.imagePath = imagePath.toString();
	}

	public void setRegionPath(String regionPath) {
		this.regionPath = regionPath + "\\";
	}

	public Image processDataFromImage() {
		Mat detectedPaperImage = null;
		Mat deskewedImage = null;
		Mat result = null;
		ArrayList<Mat> rois = null;
		
		Mat image = Imgcodecs.imread(imagePath);
		Mat base = image.clone();
		Scalar bbColor = new Scalar(0,255,0);
		PaperDetector pd = new PaperDetector(new PDDefault());
		
		Imgproc.pyrDown(base, base);
		
		detectedPaperImage = pd.paperDetect(base);
		//deskewedImage = deskew(detectedPaperImage);
		
		
		ArrayList<MatOfPoint> contours = getMatContours(detectedPaperImage);
		result = drawMatContours(base, contours, bbColor);
		//result = deskewedImage;
		
		rois = getROIFromContours(base, contours);
		ArrayList<Mat> processedROIs = processRegions(rois);
		String[] filenames = writeROIToDirectory(processedROIs, regionPath);
		
		for(int i = 0; i < filenames.length; i++) {
			try {
				System.out.println("image " + i);
				System.out.println(imageToText(filenames[i]));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		return SwingFXUtils.toFXImage(mat2Img(result), null);
	}
	
	public Image deskewImage() {
		Mat image = Imgcodecs.imread(imagePath);
		Mat base = image.clone();
		Mat result = null;
		
		result = deskew(base);
		
		
		return SwingFXUtils.toFXImage(mat2Img(result), null);
	}
	
	private Mat getImageMask(Mat image, ArrayList<MatOfPoint> contours) {
		Mat base = Mat.zeros(image.size(), CvType.CV_8UC1);
		Scalar color = new Scalar(255,255,255);
		Imgproc.drawContours(base, contours, -1, color, Core.FILLED);
		return base;
	}
	
	private Mat deskew(Mat image) {
		Mat gray = new Mat();
		Mat thresh = new Mat();
		MatOfPoint2f points = new MatOfPoint2f();
		RotatedRect rr = null;
		
		double angle = 0d;
		
		if(image.elemSize() > 1)
			Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
		else
			gray = image.clone();
		
		Core.bitwise_not(gray, gray);
		Imgproc.threshold(gray, thresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
		
		Core.findNonZero(thresh, points);
		
		MatOfPoint cvt = new MatOfPoint();
		points.convertTo(cvt, CvType.CV_32S);
		points = new MatOfPoint2f(cvt.toArray());
		
		ArrayList<MatOfPoint> contours = new ArrayList<>();
		contours.add(cvt);
		Imgproc.drawContours(thresh, contours, -1, new Scalar(0,255,0));
		
		rr = Imgproc.minAreaRect(points);
		
		
		angle = rr.angle;
		
		if (angle < -45)
			angle = (-1*(270) + angle);
		
		
		int w = image.width();
		int h = image.height();
		
		Point center = new Point(w/2, h/2);
		
		Mat m = Imgproc.getRotationMatrix2D(center, angle, 1d);
		Mat rotated = new Mat();
		Imgproc.warpAffine(image, rotated, m, new Size(w,h), Imgproc.INTER_CUBIC, Core.BORDER_REPLICATE, new Scalar(0,0,0));
		
		
		return rotated;
	}

	private Mat drawMatContours(Mat image, ArrayList<MatOfPoint> contour, Scalar color) {
		Mat base = image.clone();
		int imgWidth = base.width();
		
		int priceCounter = 0;
		int itemCounter = 0;
		
		for (int i = 0; i < contour.size(); i++) {
			Rect rect = Imgproc.boundingRect(contour.get(i));
			Imgproc.rectangle(base, rect.tl(), rect.br(), color, 2); // show contours
			
			// String text = rect.x >= imgWidth / 2 ? "price" : "item";
			int counter = rect.x >= imgWidth / 2 ? ++priceCounter : ++itemCounter;
			Imgproc.putText(base, Integer.toString(counter), rect.br(), Core.FONT_HERSHEY_SIMPLEX, 0.5, new Scalar(0,0,0));
		}

		return base;
	}
	
	private ArrayList<MatOfPoint> getMatContours(Mat image){
		Mat base = image.clone();
		ContourDetector td = new ContourDetector(new CDGradientBased());
		return td.textDetect(base);
	}
	
	private ArrayList<Mat> getROIFromContours(Mat image, ArrayList<MatOfPoint> contours){
		ArrayList<Mat> roiFromContours = new ArrayList<>();
		
		for(int i = 0; i < contours.size(); i++) {
			roiFromContours.add(image.submat(Imgproc.boundingRect(contours.get(i))));
		}
		
		return roiFromContours;
	}
	
	private String[] writeROIToDirectory(ArrayList<Mat> processedROIs, String directory) {
		final String PATH = directory;
		final String NAME = "region";
		final String IMG_EXT = ".png";
		
		String[] filenames = new String[processedROIs.size()];
		
		for(int i = 0; i < processedROIs.size(); i++) {
			filenames[i] = String.format("%s%s%d%s", PATH, NAME, i, IMG_EXT);
			Imgcodecs.imwrite(filenames[i], processedROIs.get(i));
		}
		
		return filenames;
	}

	private ArrayList<Mat> processRegions(ArrayList<Mat> rois) {
		ArrayList<Mat> matRegion = new ArrayList<>();

		for (int i = 0; i < rois.size(); i++) {
			Mat mat = rois.get(i);

			Imgproc.resize(mat, mat, new Size(0, 0), 5, 5, Imgproc.INTER_LINEAR);
			mat = unsharpMask(mat);
			Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
			Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_OTSU);
			Core.bitwise_not(mat, mat);
			//Imgproc.erode(mat, mat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
			Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_OPEN, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
			Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
			Core.bitwise_not(mat, mat);
			int top = (int) (mat.rows() * 0.1); 
			int bottom = top;
			int left =  (int) (mat.cols() * 0.1);
			int right = left;
			Core.copyMakeBorder(mat, mat, top, bottom, left, right, Core.BORDER_CONSTANT, new Scalar(255,255,255));
			mat = deskew(mat);

			matRegion.add(mat);
		}

		return matRegion;
	}
	
	private Mat processRegion(Mat roi) {
		Mat region = roi.clone();
		
		Imgproc.resize(region, region, new Size(0, 0), 3, 3, Imgproc.INTER_LINEAR);
		int top = (int) (region.rows() * 0.7); 
		int bottom = top;
		int left =  (int) (region.cols() * 0.7);
		int right = left;
		
		
		
		region = unsharpMask(region);
		Imgproc.cvtColor(region, region, Imgproc.COLOR_BGR2GRAY);
		Imgproc.threshold(region, region, 0, 255, Imgproc.THRESH_OTSU);
		Core.copyMakeBorder(region, region, top, bottom, left, right, Core.BORDER_CONSTANT, new Scalar(255,255,255));
		region = deskew(region);
		
		return region;
	}

	private Mat unsharpMask(Mat mat) {
		Mat blur = new Mat();
		Mat weighted = new Mat();
		Imgproc.GaussianBlur(mat, blur, new Size(0, 0), 3.0);
		Core.addWeighted(mat, 1.5, blur, -0.5, 0, weighted);

		return weighted;
	}

	public static Image binarize(Image image) {
		BufferedImage bimg;
		bimg = SwingFXUtils.fromFXImage(image, null);
		Mat mat = img2Mat(bimg);

		if (mat.channels() > 1) {
			Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
		}

		Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_OTSU);

		return SwingFXUtils.toFXImage(mat2Img(mat), null);
	}

	public static BufferedImage mat2Img(Mat mat) {
		int bufferSize;
		int type;
		byte[] buffer;
		byte[] targetPixels;
		BufferedImage image;

		bufferSize = mat.channels() * mat.cols() * mat.rows();
		buffer = new byte[bufferSize];

		if (mat.channels() == 1) {
			type = BufferedImage.TYPE_BYTE_GRAY;
		} else {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}

		mat.get(0, 0, buffer);
		image = new BufferedImage(mat.cols(), mat.rows(), type);
		targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);
		return image;
	}

	private static Mat img2Mat(BufferedImage in) {
		Mat out = new Mat(in.getHeight(), in.getWidth(), CvType.CV_8UC3);
		byte[] data = new byte[in.getWidth() * in.getHeight() * (int) out.elemSize()];
		int[] dataBuff = in.getRGB(0, 0, in.getWidth(), in.getHeight(), null, 0, in.getWidth());
		for (int i = 0; i < dataBuff.length; i++) {
			data[i * 3] = (byte) ((dataBuff[i]));
			data[i * 3 + 1] = (byte) ((dataBuff[i]));
			data[i * 3 + 2] = (byte) ((dataBuff[i]));
		}
		out.put(0, 0, data);
		return out;
	}

	public static String imageToText(String absolutePath) throws IOException, InterruptedException {

		
		 * List<String> commands = new ArrayList<>();
		 * commands.add("C:\\tesseract-Win64\\tesseract.exe");
		 * 
		 * commands.add("--tessdata-dir");
		 * commands.add("C:\\tesseract-Win64\\tessdata");
		 * 
		 * commands.add(absolutePath); commands.add("stdout");
		 * 
		 * ProcessBuilder build = new ProcessBuilder(commands); Process process =
		 * build.start(); BufferedReader stdInput = new BufferedReader(new
		 * InputStreamReader(process.getInputStream())); String s = ""; while ((s =
		 * stdInput.readLine()) != null) { s+=s; }
		 * 
		 * return s;
		 

		final String COMMAND = String.format(
				"C:\\tesseract-Win64\\tesseract.exe --tessdata-dir C:\\tesseract-Win64\\tessdata %s stdout -psm 6 -c textord_tabfind_find_tables=0 -c textord_all_prop=1",
				absolutePath);

		Process p = null;
		p = Runtime.getRuntime().exec(COMMAND);

		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		String line = "";
		String output = "";
		while ((line = reader.readLine()) != null) {
			output += (line + "\n");
		}

		p.waitFor();

		return output;

	}*/

}
