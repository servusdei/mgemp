package iputils.contourdetect;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class CDDefault implements ContourDetectAlgorithm {

	@Override
	public ArrayList<MatOfPoint> contourDetect(Mat image) {
		Mat base = image;
		
		Mat gray = new Mat();
		Mat gradient = new Mat();
		Mat binary = new Mat();
		Mat closed = new Mat();

		Mat hierarchy = new Mat();
		Rect rect;

		Mat kernelGrad = new Mat();
		Mat kernelClose = new Mat();

		Mat mask = null;
		
		ArrayList<MatOfPoint> contours = new ArrayList<>();
		ArrayList<MatOfPoint> filteredContours = new ArrayList<>();

		kernelGrad = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));
		kernelClose = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 1));
		
		if(image.elemSize() > 1)
			Imgproc.cvtColor(base, gray, Imgproc.COLOR_BGR2GRAY);
		else
			gray = image.clone();
		
		Imgproc.morphologyEx(gray, gradient, Imgproc.MORPH_GRADIENT, kernelGrad);
		Imgproc.threshold(gradient, binary, 0, 255, Imgproc.THRESH_OTSU);
		Imgproc.morphologyEx(binary, closed, Imgproc.MORPH_CLOSE, kernelClose);
		Imgproc.findContours(closed, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE,
				new Point(0, 0));

		mask = Mat.zeros(binary.size(), CvType.CV_8UC1);

		for (int i = 0; i < contours.size(); i++) {
			rect = Imgproc.boundingRect(contours.get(i));
			if (rect.height < 10 || rect.width < 10)
				continue;
			// if(rect.height > rect.width) continue;

			Mat maskROI = new Mat(mask, rect);
			Imgproc.drawContours(mask, contours, i, new Scalar(255, 255, 255), Core.FILLED);
			double r = (double) Core.countNonZero(maskROI) / (rect.width * rect.height);

			if (r > 0.45)
				filteredContours.add(contours.get(i));
			// Imgproc.rectangle(base, rect.tl(), rect.br(), new Scalar(0, 255, 0), 2); //
			// show contours
		}

		return filteredContours;
	}
	
	

}
