package iputils.contourdetect;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;

public class ContourDetector {
	
	private ContourDetectAlgorithm cda;
	
	public ContourDetector(ContourDetectAlgorithm cda) {
		this.cda = cda;
	}
	
	public ArrayList<MatOfPoint> textDetect(Mat image) {
		return cda.contourDetect(image);
	}
	
	

}
