package iputils.contourdetect;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class CDGradientBased implements ContourDetectAlgorithm {

	@Override
	public ArrayList<MatOfPoint> contourDetect(Mat image) {
		Mat base = image;
		Mat rgb = new Mat();
		Mat small = new Mat();
		Mat bw = new Mat();
		
		Mat grad = new Mat();
		Mat morphKernel1 = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3,3));
		Mat morphKernel2 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(getHorizontalWindowSize(image), 1)); //new Size(9, 1) original to 50,1
		
		Mat connected = new Mat();
		
		MatOfInt4 hierarchy = new MatOfInt4();
		
		ArrayList<MatOfPoint> contours = new ArrayList<>();
		ArrayList<MatOfPoint> filteredContours = new ArrayList<>();
		
		// Imgproc.pyrDown(base, rgb);
		rgb = base;
		Imgproc.cvtColor(rgb, small, Imgproc.COLOR_BGR2GRAY);
		Imgproc.morphologyEx(small, grad, Imgproc.MORPH_GRADIENT, morphKernel1);
		Imgproc.threshold(grad, bw, 0, 255, Imgproc.THRESH_OTSU);
		Imgproc.morphologyEx(bw, connected, Imgproc.MORPH_CLOSE, morphKernel2);
		
		
		Mat mask = Mat.zeros(bw.size(), CvType.CV_8UC1);
		
		Imgproc.findContours(connected, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));
		
		for (int i = 0; i < contours.size(); i++) {
			Rect rect = Imgproc.boundingRect(contours.get(i));
			Mat maskROI = new Mat(mask, rect);
			Imgproc.drawContours(mask, contours, i, new Scalar(255,255,255), Core.FILLED);
			double r = (double)Core.countNonZero(maskROI)/(rect.width*rect.height);
			
			if(r > 0.45 && (rect.height > 8 && rect.width > 8)) {
				filteredContours.add(contours.get(i));
			}
		}
		
		return filteredContours;
	}
	
	private int getHorizontalWindowSize(Mat image) {
		int windowSize = 100;
		int baseImageWidth = 3456;
		
		while(image.width() < baseImageWidth) {
			windowSize /= 2;
			baseImageWidth /= 2;
		}
		
		
		return windowSize;
	}

}
