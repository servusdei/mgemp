package iputils.contourdetect;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;

interface ContourDetectAlgorithm {
	public ArrayList<MatOfPoint> contourDetect(Mat image);
}
