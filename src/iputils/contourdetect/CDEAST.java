package iputils.contourdetect;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgcodecs.Imgcodecs;

public class CDEAST implements ContourDetectAlgorithm {
	
	public static void main(String[] argv) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		new CDEAST().contourDetect(Imgcodecs.imread("C:\\Users\\Deogracias Aaron\\Pictures\\receipt.jpg"));
	}

	@Override
	public ArrayList<MatOfPoint> contourDetect(Mat image) {
		Mat orig = image.clone();
		final int H = image.height();
		final int W = image.width();
		
		ArrayList<String> layerNames = new ArrayList<>();
		layerNames.add("feature_fusion/Conv_7/Sigmoid");
		layerNames.add("feature_fusion/concat_3");
		
		Net net = Dnn.readNet("E:\\Repository\\eclipse-oxygen-3\\mgemp\\frozen_east_text_detection.pb");
		Mat blob = Dnn.blobFromImage(image, 1d, new Size(image.width(), image.height()), new Scalar(123.68, 116.78, 103.94), true, false);
		net.setInput(blob);
		ArrayList<Mat> outputBlobs = new ArrayList<>();
		net.forward(outputBlobs, layerNames);
		
		System.out.println(outputBlobs.size());
		
		
		return null;
	}



}
