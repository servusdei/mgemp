package iputils.contourdetect;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.features2d.FastFeatureDetector;
import org.opencv.imgproc.Imgproc;

public class CDFastFeatureDetector implements ContourDetectAlgorithm {

	@Override
	public ArrayList<MatOfPoint> contourDetect(Mat image) {
		Mat mRgba = image;
		Mat mGray = new Mat();
		
		if(image.elemSize() > 1)
			Imgproc.cvtColor(mRgba, mGray, Imgproc.COLOR_RGB2GRAY);
		else
			mGray = image.clone();

		Scalar CONTOUR_COLOR = new Scalar(1, 255, 128, 0);
		MatOfKeyPoint keyPoint = new MatOfKeyPoint();
		List<KeyPoint> listPoint = new ArrayList<>();
		KeyPoint kPoint = new KeyPoint();
		Mat mask = Mat.zeros(mGray.size(), CvType.CV_8UC1);
		int rectanx1;
		int rectany1;
		int rectanx2;
		int rectany2;

		List<MatOfPoint> contour2 = new ArrayList<>();
		Mat kernel = new Mat(1, 50, CvType.CV_8UC1, Scalar.all(255));
		Mat morByte = new Mat();
		Mat hierarchy = new Mat();

		ArrayList<MatOfPoint> filteredContours = new ArrayList<>();

		FastFeatureDetector detector = FastFeatureDetector.create();
		detector.detect(mGray, keyPoint);
		listPoint = keyPoint.toList();
		for (int ind = 0; ind < listPoint.size(); ++ind) {
			kPoint = listPoint.get(ind);
			rectanx1 = (int) (kPoint.pt.x - 0.5 * kPoint.size);
			rectany1 = (int) (kPoint.pt.y - 0.5 * kPoint.size);

			rectanx2 = (int) (kPoint.size);
			rectany2 = (int) (kPoint.size);
			if (rectanx1 <= 0) {
				rectanx1 = 1;
			}
			if (rectany1 <= 0) {
				rectany1 = 1;
			}
			if ((rectanx1 + rectanx2) > mGray.width()) {
				rectanx2 = mGray.width() - rectanx1;
			}
			if ((rectany1 + rectany2) > mGray.height()) {
				rectany2 = mGray.height() - rectany1;
			}
			Rect rectant = new Rect(rectanx1, rectany1, rectanx2, rectany2);
			Mat roi = new Mat(mask, rectant);
			roi.setTo(CONTOUR_COLOR);
		}
		Imgproc.morphologyEx(mask, morByte, Imgproc.MORPH_DILATE, kernel);
		Imgproc.findContours(morByte, contour2, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
		
		/*for (int i = 0; i < contour2.size(); ++i) {
			rectan3 = Imgproc.boundingRect(contour2.get(i));
			if (rectan3.area() > 0.5 * imgSize || rectan3.area() < 100 || rectan3.width / rectan3.height < 2) {
				Mat roi = new Mat(morByte, rectan3);
				roi.setTo(zeros);
			} else {
				filteredContours.add(contour2.get(i));
			}
		}*/
		
		for (int i = 0; i < contour2.size(); i++) {
			Rect rect = Imgproc.boundingRect(contour2.get(i));
			if (rect.height < 10 || rect.width < 10)
				continue;
			// if(rect.height > rect.width) continue;

			Mat maskROI = new Mat(mask, rect);
			Imgproc.drawContours(mask, contour2, i, new Scalar(255, 255, 255), Core.FILLED);
			double r = (double) Core.countNonZero(maskROI) / (rect.width * rect.height);

			if (r > 0.45)
				filteredContours.add(contour2.get(i));
			// Imgproc.rectangle(base, rect.tl(), rect.br(), new Scalar(0, 255, 0), 2); //
			// show contours
		}

		return filteredContours;
	}

}
