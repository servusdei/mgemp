package iputils.regions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class RegionOfInterest {
	
	private Mat matRegion;
	private int x;
	private int y;
	private File asFile;
	
	public RegionOfInterest(Mat matRegion, int x, int y) {
		this.matRegion = matRegion;
		this.x = x;
		this.y = y;
	}
	
	public void setMatRegion(Mat matRegion) {
		this.matRegion = matRegion;
	}

	public Mat getMatRegion() {
		return matRegion;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public File getAsFile() {
		return asFile;
	}

	public void setAsFile(File asFile) {
		this.asFile = asFile;
	}

	public static ArrayList<Mat> generateMatRegionsFromROIList(List<RegionOfInterest> rois){
		ArrayList<Mat> listOfMatRegions = new ArrayList<>();
		
		for(int i = 0; i < rois.size(); i++) {
			listOfMatRegions.add(rois.get(i).getMatRegion());
		}
		
		return listOfMatRegions;
	}
	
	public static void writeROIsToDirectory(List<RegionOfInterest> rois, File directory, String basename) {
		final String PATH = directory.getAbsolutePath() + "\\";
		final String NAME = basename;
		final String IMG_EXT = ".png";
		
		for(int i = 0; i < rois.size(); i++) {
			String file = String.format("%s%s%d%s", PATH, NAME, i, IMG_EXT);
			rois.get(i).setAsFile(new File(file));
			Imgcodecs.imwrite(file, rois.get(i).getMatRegion());
		}
		
	}
	
	public static void writeLargeROIsToDirectory(List<RegionOfInterest> rois, File directory, String basename, int iterations) {
		final String PATH = directory.getAbsolutePath() + "\\";
		final String NAME = basename;
		final String IMG_EXT = ".png";
		
		for(int i = 0; i < rois.size(); i++) {
			String file = String.format("%s%s%d%s", PATH, NAME, i, IMG_EXT);
			rois.get(i).setAsFile(new File(file));
			Mat large = rois.get(i).getMatRegion().clone();
			for(int j = 0; j < iterations; j++) Imgproc.pyrUp(large, large);
			Imgcodecs.imwrite(file, large);
		}
		
	}

}
