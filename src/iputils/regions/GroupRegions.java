package iputils.regions;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class GroupRegions {
	
	public static Mat groupMask(Mat image, ArrayList<MatOfPoint> contours) {
		
		Mat bw = new Mat();
		Imgproc.cvtColor(image, bw, CvType.CV_8UC1);
		
		Mat mask = Mat.zeros(bw.size(), CvType.CV_8UC1);
		
		for(int i = 0; i < contours.size(); i++) {
			Rect rect = Imgproc.boundingRect(contours.get(i));
			Imgproc.rectangle(mask, rect.tl(), rect.br(), new Scalar(255, 255, 255), Core.FILLED); // show contours
		}
		
		
		
		
		return close(mask);
		
	}
	
	private static Mat dilate(Mat image) {
		Mat dilate = new Mat();
		Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, 20));
		
		Imgproc.dilate(image, dilate, kernel);
		
		return dilate;
	}
	
	private static Mat close(Mat image) {
		Mat close = new Mat();
		Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, 20));
		
		Imgproc.morphologyEx(image, close, Imgproc.MORPH_CLOSE, kernel);
		
		return close;
	}

}
