package iputils.contourcategorize;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;

public interface ContourCategorizeAlgorithm {
	public ArrayList<ArrayList<MatOfPoint>> contourCategorize(Mat image, ArrayList<MatOfPoint> contours);
}
