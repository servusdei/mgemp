package iputils.contourcategorize;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;

public class ContourCategorizer {
	private ContourCategorizeAlgorithm cca;
	
	public ContourCategorizer(ContourCategorizeAlgorithm cca) {
		this.cca = cca;
	}
	
	public ArrayList<ArrayList<MatOfPoint>> categorizeContour(Mat image, ArrayList<MatOfPoint> contours) {
		return cca.contourCategorize(image, contours);
	}
}
