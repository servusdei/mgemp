package iputils.paperdetect;

import org.opencv.core.Mat;

interface PaperDetectAlgorithm {
	
	public Mat getMatPolygonalRegion(Mat image);

}
