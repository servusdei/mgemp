package iputils.paperdetect;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class PDDefault implements PaperDetectAlgorithm {
	
	private static MatOfPoint getLargestContour(ArrayList<MatOfPoint> contours) {
		double largestArea = 0;
		int largestAreaIdx = 0;
		
		for (int i = 0; i < contours.size(); i++) {
			double contourArea = Imgproc.contourArea(contours.get(i));
			
			if(largestArea < contourArea) {
				largestArea = contourArea;
				largestAreaIdx = i;
			}
		}
		
		return contours.get(largestAreaIdx);
	}


	@Override
	public Mat getMatPolygonalRegion(Mat image) {
		Mat base = image;
		Mat hsv = new Mat();
		Mat thresh = new Mat();
		
		ArrayList<Mat> hsvChannels = new ArrayList<>();
		ArrayList<MatOfPoint> contours = new ArrayList<>();
		
		MatOfPoint largestContour = new MatOfPoint();
		
		Imgproc.cvtColor(base, hsv, Imgproc.COLOR_BGR2HSV);
		Core.split(hsv, hsvChannels);
		Imgproc.threshold(hsvChannels.get(1), thresh, 50, 255, Imgproc.THRESH_BINARY_INV);
		Imgproc.findContours(thresh, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		
		largestContour = getLargestContour(contours);
		
		/*MatOfPoint2f curve = new MatOfPoint2f(largestContour.toArray());
		double arclen = Imgproc.arcLength(curve, true);
		MatOfPoint2f approxCurve = new MatOfPoint2f();
		Imgproc.approxPolyDP(curve, approxCurve, arclen * 0.02, true);*/
		
		Mat canvas = base.clone();
		
		ArrayList<MatOfPoint> cont = new ArrayList<MatOfPoint>();
		cont.add(largestContour);
		
		/*MatOfPoint m = new MatOfPoint();
		approxCurve.convertTo(m, CvType.CV_32S);
		ArrayList<MatOfPoint> approx = new ArrayList<MatOfPoint>();
		approx.add(m);*/
		
		Mat mask = new Mat();
		
		Imgproc.drawContours(canvas, cont, -1, new Scalar(255,255,255), Core.FILLED);
		
		Mat dst = new Mat();
		Imgproc.threshold(canvas, mask, 254, 255, Imgproc.THRESH_BINARY);
		// Core.bitwise_and(canvas, src2, dst);
		Core.bitwise_and(base, mask, dst);
		//Imgproc.drawContours(canvas, approx, -1, new Scalar(0,0,255));
		
		return dst;
	}

}
