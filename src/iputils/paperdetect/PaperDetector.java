package iputils.paperdetect;

import org.opencv.core.Mat;

public class PaperDetector {
	
	private PaperDetectAlgorithm pda;
	
	public PaperDetector(PaperDetectAlgorithm pda) {
		this.pda = pda;
	}
	
	public Mat paperDetect(Mat image) {
		return pda.getMatPolygonalRegion(image);
	}

}
