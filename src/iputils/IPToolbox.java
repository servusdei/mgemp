package iputils;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.imgproc.Imgproc;

import iputils.contourdetect.CDGradientBased;
import iputils.contourdetect.ContourDetector;
import iputils.paperdetect.PDDefault;
import iputils.paperdetect.PaperDetector;
import iputils.regions.RegionOfInterest;

public abstract class IPToolbox {
	
	
	public static ArrayList<Mat> getROIFromContoursAsMat(Mat image, ArrayList<MatOfPoint> contours){
		ArrayList<Mat> roiFromContours = new ArrayList<>();
		
		for(int i = 0; i < contours.size(); i++) {
			roiFromContours.add(image.submat(Imgproc.boundingRect(contours.get(i))));
		}
		
		return roiFromContours;
	}
	
	public static ArrayList<RegionOfInterest> getROIFromContoursAsROIs(Mat image, List<MatOfPoint> contours){
		ArrayList<RegionOfInterest> roiFromContours = new ArrayList<>();
		
		for(int i = 0; i < contours.size(); i++) {
			Rect r = Imgproc.boundingRect(contours.get(i));
			RegionOfInterest roi = new RegionOfInterest(image.submat(r), r.x, r.y);
			roiFromContours.add(roi);
		}
		
		return roiFromContours;
	}

	
	public static ArrayList<MatOfPoint> getMatContours(Mat image) {
		ContourDetector cd = new ContourDetector(new CDGradientBased());
		ArrayList<MatOfPoint> temp = cd.textDetect(image);
		Collections.reverse(temp);
		return temp;
	}
	
	public static Mat getPaperDetectMask(Mat image) {
		PaperDetector pd = new PaperDetector(new PDDefault());
		return pd.paperDetect(image);
	}
	
	public static Mat drawMatContours(Mat image, List<MatOfPoint> contours, Scalar color) {
		Mat base = image.clone();
		int imgWidth = base.width();
		
		int priceCounter = 0;
		int itemCounter = 0;
		
		for (int i = 0; i < contours.size(); i++) {
			Rect rect = Imgproc.boundingRect(contours.get(i));
			Imgproc.rectangle(base, rect.tl(), rect.br(), color, 2); // show contours
			
			// String text = rect.x >= imgWidth / 2 ? "price" : "item";
			int counter = rect.x >= imgWidth / 2 ? ++priceCounter : ++itemCounter;
			String str = rect.x >= imgWidth / 2 ? "price " : "item ";
			String coords = String.format(" (%d, %d)", rect.x, rect.y);
			Imgproc.putText(base, str + Integer.toString(counter) + coords, rect.br(), Core.FONT_HERSHEY_SIMPLEX, 0.5, new Scalar(0,0,0));
		}

		return base;
	}
	
	public static Mat deskew(Mat image) {
		Mat gray = new Mat();
		Mat thresh = new Mat();
		MatOfPoint2f points = new MatOfPoint2f();
		RotatedRect rr = null;
		
		double angle = 0d;
		
		if(image.channels() > 1)
			Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
		else
			gray = image.clone();
		
		Core.bitwise_not(gray, gray);
		Imgproc.threshold(gray, thresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
		
		Core.findNonZero(thresh, points);
		
		MatOfPoint cvt = new MatOfPoint();
		points.convertTo(cvt, CvType.CV_32S);
		points = new MatOfPoint2f(cvt.toArray());
		
		/*ArrayList<MatOfPoint> contours = new ArrayList<>();
		contours.add(cvt);
		Imgproc.drawContours(thresh, contours, -1, new Scalar(0,255,0));*/
		
		rr = Imgproc.minAreaRect(points);
		
		
		angle = rr.angle;
		
		if (angle < -45)
			angle = (-1*(270) + angle);
		
		
		int w = image.width();
		int h = image.height();
		
		Point center = new Point(w/2, h/2);
		
		Mat m = Imgproc.getRotationMatrix2D(center, angle, 1d);
		Mat rotated = new Mat();
		Imgproc.warpAffine(image, rotated, m, new Size(w,h), Imgproc.INTER_CUBIC, Core.BORDER_REPLICATE, new Scalar(0,0,0));
		
		
		return rotated;
	}
	
	public static Mat unsharpMask(Mat mat, Size kSize, double sigmaX) {
		Mat blur = new Mat();
		Mat weighted = new Mat();
		Imgproc.GaussianBlur(mat, blur, kSize, sigmaX);
		Core.addWeighted(mat, 1.5, blur, -0.5, 0, weighted);

		return weighted;
	}
	
	public static Mat minMaxContrastStretching(Mat mat) {
		if(mat.channels() > 1)	Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
		Mat minima = new Mat();
		Mat maxima = new Mat();
		
		MinMaxLocResult res = Core.minMaxLoc(mat);
		double maxVal = res.maxVal;
		double minVal = res.minVal;
		
		Mat minMat = new Mat(mat.size(), CvType.CV_8UC1, new Scalar(minVal));
		Mat maxMat = new Mat(mat.size(), CvType.CV_8UC1, new Scalar(maxVal));
		
		Core.subtract(mat, minMat, minima);
		Core.subtract(mat, maxMat, maxima);
		
		Core.divide(minima, maxima, mat);
		Core.multiply(mat, new Scalar(255), mat);
		
		return mat;
	}
	
	public static BufferedImage mat2Img(Mat mat) {
		int bufferSize;
		int type;
		byte[] buffer;
		byte[] targetPixels;
		BufferedImage image;

		bufferSize = mat.channels() * mat.cols() * mat.rows();
		buffer = new byte[bufferSize];

		if (mat.channels() == 1) {
			type = BufferedImage.TYPE_BYTE_GRAY;
		} else {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}

		mat.get(0, 0, buffer);
		image = new BufferedImage(mat.cols(), mat.rows(), type);
		targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);
		return image;
	}
	

}
