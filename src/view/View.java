package view;

import java.io.File;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class View {

	private Stage stage;
	private Scene scene;

	private Group root;
	private ScrollPane sp;
	private ImageView imageview;

	private Button btnOpenImg;
	private Button btnProcessImg;

	private static final int ivW = 800;
	private static final int ivH = 600;

	public View(Stage stage) {
		this.stage = stage;
		this.stage.setTitle("ImageView");

		constructRoot();

		constructImageview();
		constructBtnOpenImg();
		constructBtnProcessImg();

		addToRoot();
		constructScene();

		this.stage.setScene(scene);
	}

	private void constructRoot() {
		root = new Group();
	}

	private void constructImageview() {
		imageview = new ImageView();
		imageview.prefWidth(ivW);
		imageview.prefHeight(ivH);
	}

	private void constructBtnOpenImg() {
		btnOpenImg = new Button("Open Image");
		btnOpenImg.setLayoutX((ivW / 2) - 50);
		btnOpenImg.setLayoutY((ivH));
	}

	private void constructBtnProcessImg() {
		btnProcessImg = new Button("Process Image");
		btnProcessImg.setLayoutX((ivW / 2) + 50);
		btnProcessImg.setLayoutY((ivH));
	}

	private void addToRoot() {
		sp = new ScrollPane();
		sp.setPrefSize(800, 600);
		sp.setContent(imageview);
		root.getChildren().addAll(sp, btnOpenImg, btnProcessImg);
	}

	private void constructScene() {
		scene = new Scene(root, 1366, 768);
	}

	public void show() {
		stage.show();
	}

	public Button getBtnOpenImg() {
		return btnOpenImg;
	}

	public Button getBtnProcessImg() {
		return btnProcessImg;
	}

	public File invokeFileChooser() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		File file = fileChooser.showOpenDialog(stage);
		return file;
	}
	
	public void setImageviewImage(String path) {
		if(!path.equals("")) {
			Image image = new Image(path);
			imageview.setImage(image);
		}
		else {
			imageview.setImage(null);
		}
		
	}
	
	public void setImageviewImage(Image img) {
		Image image = img;
		imageview.setImage(image);
	}

}
