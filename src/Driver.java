import org.opencv.core.Core;

import controller.MainController;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Model;
import view.View;

public class Driver extends Application {

	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		/*System.load(String.format("%s\\%s%s", 
				new File("").getAbsolutePath(), 
				Core.NATIVE_LIBRARY_NAME,
				".dll"));*/
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Model model = new Model();
		View view = new View(stage);
		
		MainController.getInstance(model, view);
		view.show();
		
	}

}
